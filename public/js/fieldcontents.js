$(document).ready(function(){
	$(".disciplinaSelect").on('change', function(){
		disciplinas_id = $(".disciplinaSelect").val()
		$.get('/instructores-by-disciplina/'+disciplinas_id+'', function(response){
			if(response.length > 0){
				content = ''
				for(i = 0; i < response.length; i++)
					content += '<option value="'+response[i].id+'">'+response[i].nombre_i+' '+response[i].apellido_i+'</option>'
				instructorSelect = $(".instructorSelect")
				if(instructorSelect) instructorSelect.html(content)
			}
		});
	});
	$(".disciplinaSelectInscripcion").on('change',function(){
		disciplinas_id = $(".disciplinaSelectInscripcion").val()
		$.get('/horarios-by-disciplina/'+disciplinas_id+'',function(response){
			$("#instructorTableInfo").remove()
			if(response.length > 0){
				content = ''
				for(i = 0; i < response.length; i++){
					content += '<tr><td>'+response[i].nombre_i+' '+response[i].apellido_i+'</td>'
					content += '<td>'+response[i].hr_ini+'</td>'
					content += '<td>'+response[i].hr_fin+'</td>'
					content += '<td align="left"><input name="horarios_disc_id" type="radio" value="'+response[i].id+'"></td></tr>'
				}
				instructorTBody = $(".instructorTable")
				instructorTBody.html(content)
			}
		});
	});
	$(".search-cliente1").on('click', function(){
		search_target = $(".search1").val()
		div_target = $("#searchResults1")
		tbody_target = $(".resultadosTable")
		tbody_content = ''
		div_info = $("#clientesTableInfo")
		if(search_target != ""){
			$.get('/clientes/search/'+search_target+'',function(response){
			console.log(response)
			if(response.length > 0){
				div_info.hide()
				for(i = 0; i < response.length; i++){
					tbody_content += '<tr><td>'+response[i].nombre_c+'</td>'
					tbody_content += '<td>'+response[i].apellido_c+'</td>'
					tbody_content += '<td>'+response[i].celular_c+'</td>'
					tbody_content += '<td align="left"><input name="clientes_id" type="radio" value="'+response[i].id+'"></td></tr>'
				}
				tbody_target.html(tbody_content)
				//borramos los date fields si ya se hizo busquedas anteriores
				$(".fechasFields1").remove()
				//agregamos los inputs de fecha ini, fin
				date_fields = '<div class="col-md-12 fechasFields1"><br><h5>Fechas de inscripci&oacute;n e importe</h5><hr></div><div class="row col-md-12 fechasFields1"><div class="col-md-4"><label for="fecha_ini">Fecha inicio</label><input class="form-control fecha_ini2" placeholder="Fecha de inicio de la inscripci&oacute;n" name="fecha_ini2" type="text"></div><div class="col-md-4"><label for="fecha_fin">Fecha fin</label><input class="form-control fecha_fin2" placeholder="Fecha de fin de la inscripci&oacute;n" name="fecha_fin2" type="text"></div><div class="col-md-4"><label for="importe">Importe</label><input class="form-control" placeholder="Ingrese el monto" name="importe2" type="text" id="importe"></div></div>'
				$("#searchResults1").after(date_fields)
				$('.fecha_ini2').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
				$('.fecha_fin2').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
			}else{
				tbody_target.html("")
				div_info.removeClass('alert alert-info')
				div_info.addClass('alert alert-danger')
				div_info.html('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-times-circle"></i> No se encontraron resultados.</strong>')
				div_info.show()
			}
			});
			
		}
	});
	$(document).on('click','.agregarHorarioForm1', function(){
		template = '<button type="button" class="btn btn-link quitarHorarioForm1"><i class="fa fa-minus"></i>&nbsp; Agregar horario luego </button><div class="row horarioForm1"><div class="col-md-5"><label for="hr_ini">Hora de inicio</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></div><input type="text" name="hr_ini" class="form-control hr_ini2" placeholder="Haga click aquí para elegir una hora" data-dtp="dtp_GqOuK"></div></div><div class="col-md-5"><label for="hr_fin">Hora de finalización</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></div><input type="text" name="hr_fin" class="form-control hr_fin2" placeholder="Haga click aquí para elegir una hora" data-dtp="dtp_1oOEm"></div></div></div><br class="spaceJump2"><br class="spaceJump2">'
		$(".agregarHorarioForm1").after(template)
		$(".agregarHorarioForm1").remove()
		$(".spaceJump1").remove()
		$('.hr_ini2').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
		$('.hr_fin2').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
		
	});
	$(document).on('click','.quitarHorarioForm1', function(){
			$(".horarioForm1").remove()
			$(".quitarHorarioForm1").remove()
			$(".spaceJump2").remove()
			$(".instructorMainForm1").after('<button type="button" class="btn btn-link agregarHorarioForm1"><i class="fa fa-plus"></i>&nbsp; Asignar horario para este instructor</button><br class="spaceJump1"><br class="spaceJump1">')
		});
	//DATETIME PICKER
	$('#hr_ini1').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
	$('#hr_fin1').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
	$('#fecha_ini1').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
	$('#fecha_fin1').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
	//DATETIME PICKER
	//styles
	$(function() {
    // Input radio-group visual controls
	    $('.radio-group label').on('click', function(){
	        $(this).removeClass('not-active').siblings().addClass('not-active');
	    });
	});
	//styles
});