<?php

namespace GymAdmin\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use GymAdmin\Inscripcion;
use Log;

class VerificarFechas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clientes:verificarfechas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar caducidad de fechas de la inscripcion.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inscripciones = Inscripcion::all();
        $fecha_de_hoy = Carbon::now('America/La_Paz')->toDateString();
        foreach($inscripciones as $inscripcion){
            if($fecha_de_hoy > $inscripcion->fecha_fin){
                Log::info('El comando se ejecuto !');
                $inscripcion->estado = 'caducado';
                $inscripcion->save();
            }

                
        }
        
    }
}
