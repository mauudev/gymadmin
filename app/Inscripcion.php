<?php

namespace GymAdmin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Inscripcion extends Model
{
    use SoftDeletes;
    protected $table = 'inscripciones';
    protected $dates = ['deleted_at'];
    protected $fillable = ['clientes_id',
	  					   'usuarios_id',
	  					   'horarios_disc_id',
	  					   'fecha_ini',
	  					   'fecha_fin',
                           'importe',
	  					   'estado',
                           'deleted_at'];

    public static function getAllInscripciones(){
        return DB::table('inscripciones')
                ->select('inscripciones.id','inscripciones.fecha_ini','inscripciones.fecha_fin',
                         'inscripciones.estado','inscripciones.importe','clientes.nombre_c',
                         'clientes.apellido_c','clientes.celular_c','clientes.genero',
                         'usuarios.nombre_u','usuarios.apellido_u','usuarios.celular_u',
                         'horarios_disc.id as horarios_disc_id','horarios_disc.instructores_id',
                         'disciplinas.nombre_d')
                ->join('clientes','clientes.id','=','inscripciones.clientes_id','left outer')
                ->join('usuarios','usuarios.id','=','inscripciones.usuarios_id','left outer')
                ->join('horarios_disc','horarios_disc.id','=','inscripciones.horarios_disc_id','left outer')
                ->join('disciplinas','horarios_disc.disciplinas_id','=','disciplinas.id','left outer')
                ->where('inscripciones.deleted_at','=',null)
                ->orderBy('inscripciones.created_at','desc')
                ->paginate(15);
    }

    public static function getInscripcionById($id){
        return DB::table('inscripciones')
                ->select('inscripciones.id','inscripciones.fecha_ini','inscripciones.fecha_fin',
                         'inscripciones.estado','inscripciones.importe','clientes.id as clientes_id',
                         'clientes.nombre_c','clientes.apellido_c','clientes.celular_c','clientes.genero',
                         'usuarios.id as usuarios_id','usuarios.nombre_u','usuarios.apellido_u',
                         'usuarios.celular_u','horarios_disc.id as horarios_disc_id',
                         'horarios_disc.instructores_id','horarios_disc.hr_ini','horarios_disc.hr_fin',
                         'disciplinas.id as disciplinas_id','disciplinas.nombre_d','instructores.nombre_i',
                         'instructores.apellido_i')
                ->join('clientes','clientes.id','=','inscripciones.clientes_id','left outer')
                ->join('usuarios','usuarios.id','=','inscripciones.usuarios_id','left outer')
                ->join('horarios_disc','horarios_disc.id','=','inscripciones.horarios_disc_id','left outer')
                ->join('disciplinas','horarios_disc.disciplinas_id','=','disciplinas.id','left outer')
                ->join('instructores','horarios_disc.instructores_id','=','instructores.id')
                ->where('inscripciones.id','=',$id)
                ->first();

    }
}
