<?php

namespace GymAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Disciplina extends Model
{
    use SoftDeletes;
    protected $table = 'disciplinas';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre_d',	
    					   'descripcion',
                           'deleted_at'];

    public function setNombreDAttribute($value){
    	$this->attributes['nombre_d'] = ucfirst($value);
    }
    public function setDescripcionAttribute($value){
    	$this->attributes['descripcion'] = ucfirst($value);
    }
}
