<?php

namespace GymAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Instructor extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'instructores';
    protected $fillable = ['nombre_i',
              					   'apellido_i',
              					   'celular_i',
                           'disciplinas_id',
                           'deleted_at'];

    public function setNombreIAttribute($value){
    	 $this->attributes['nombre_i'] = ucwords($value);
    } 
    public function setApellidoIAttribute($value){
    	 $this->attributes['apellido_i'] = ucwords($value);
    } 

    public static function getAllInstructores(){
        return DB::table('instructores')
            ->select('instructores.id','instructores.disciplinas_id',
                     'instructores.nombre_i','instructores.apellido_i',
                     'instructores.celular_i','disciplinas.nombre_d')
            ->join('disciplinas','instructores.disciplinas_id','=','disciplinas.id','left outer')
            ->where('instructores.deleted_at', '=',null)
            ->orderBy('instructores.updated_at','desc')
            ->paginate(15);
    }

    public static function getInstructoresByDisciplina($disciplinas_id){
        return Instructor::where('disciplinas_id','=',$disciplinas_id)->get();
    }
}
