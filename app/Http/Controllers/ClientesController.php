<?php

namespace GymAdmin\Http\Controllers;

use Illuminate\Http\Request;
use GymAdmin\Cliente;
use Session;
use DB;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function busquedaCliente(Request $request, $search){
        if($request->ajax()){
            $clientes = Cliente::searchClientes($search);
            return response()->json($clientes);
        }else{
            $clientes = Cliente::searchClientes($search);
            return $clientes;
        }
    }
    
    public function index()
    {
        
        $clientes = Cliente::paginate(10);
        $coleccion = collect($clientes->all());
        if($coleccion->count() <= 0)
            Session::flash('errors-message','no se encontraron registros !');
        return view('clientes.index',['clientes'=>$clientes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente = Cliente::create($request->all());
        Session::flash('success-message','cliente registrado correctamente !');
        return redirect()->route('clientes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        return view('clientes.edit',['cliente'=>$cliente]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        $cliente->nombre_c = $request->nombre_c;
        $cliente->apellido_c = $request->apellido_c;
        $cliente->celular_c = $request->celular_c;
        $cliente->genero = $request->genero;
        $cliente->save();
        Session::flash('success-message','datos actualizados correctamente !');
        return redirect()->route('clientes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);
        $cliente->delete();
        Session::flash('success-message','datos eliminados correctamente !');
        return redirect()->route('clientes.index');
    }
}
