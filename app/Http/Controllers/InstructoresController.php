<?php

namespace GymAdmin\Http\Controllers;

use Illuminate\Http\Request;
use GymAdmin\Instructor;
use GymAdmin\Disciplina;
use GymAdmin\HorarioDisc;
use Session;

class InstructoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInstructoresByDisciplina(Request $request, $disciplinas_id){
        if($request->ajax()){
            $instructores = Instructor::getInstructoresByDisciplina($disciplinas_id);
            return response()->json($instructores);
        }else{
            $instructores = Instructor::getInstructoresByDisciplina($disciplinas_id);
            return $instructores;
        }
    }
    
    public function index()
    {
        $instructores = Instructor::getAllInstructores();
        if(count($instructores->all()) <= 0)
            Session::flash('errors-message','no se encontraron resultados !');
        return view('instructores.index',['instructores'=>$instructores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disciplinas = Disciplina::pluck('nombre_d','id');
        return view('instructores.create',['disciplinas'=>$disciplinas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $instructor = Instructor::create($request->all());
        if(isset($request->hr_ini) && isset($request->hr_fin)){
            $horario_instructor = new HorarioDisc();
            $horario_instructor->instructores_id = $instructor->id;
            $horario_instructor->disciplinas_id = $request->disciplinas_id;
            $horario_instructor->hr_ini = $request->hr_ini;
            $horario_instructor->hr_fin = $request->hr_fin;
            $horario_instructor->save();
        }
        Session::flash('success-message','datos registrados correctamente !');
        return redirect()->route('instructores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instructor = Instructor::find($id);
        $disciplinas = Disciplina::pluck('nombre_d','id');
        return view('instructores.edit',['instructor'=>$instructor,'disciplinas'=>$disciplinas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $instructor = Instructor::find($id);
        $instructor->nombre_i = $request->nombre_i;
        $instructor->apellido_i = $request->apellido_i;
        $instructor->celular_i = $request->celular_i;
        $instructor->disciplinas_id = $request->disciplinas_id;
        $instructor->save();
        Session::flash('success-message','datos actualizados correctamente !');
        return redirect()->route('instructores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instructor = Instructor::find($id);
        $instructor->delete();
        Session::flash('success-message','datos eliminados correctamente !');
        return redirect()->route('instructores.index');
    }
}
