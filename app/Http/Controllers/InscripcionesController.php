<?php

namespace GymAdmin\Http\Controllers;

use Illuminate\Http\Request;
use GymAdmin\Inscripcion;
use GymAdmin\Usuario;
use GymAdmin\Disciplina;
use GymAdmin\Cliente;
use GymAdmin\HorarioDisc;
use Session;

class InscripcionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inscripciones = Inscripcion::getAllInscripciones();
        $coleccion = collect($inscripciones->all());
        if($coleccion->count() <= 0)
            Session::flash('errors-message','no se encontraron resultados !');
        return view('inscripciones.index',['inscripciones'=>$inscripciones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $horarios = HorarioDisc::getAllHorariosCreateForm();
        $disciplinas = Disciplina::pluck('nombre_d','id');
        return view('inscripciones.create',['horarios'=>$horarios,'disciplinas'=>$disciplinas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        if($request->nombre_c != null || $request->apellido_c != null || $request->celular_c != null){
            $cliente = Cliente::create($request->all());
            $inscripcion = new Inscripcion();
            $inscripcion->clientes_id = $cliente->id;
            $inscripcion->usuarios_id = auth()->user()->id;
            $inscripcion->horarios_disc_id = $request->horarios_disc_id;
            $inscripcion->fecha_ini = $request->fecha_ini;
            $inscripcion->fecha_fin = $request->fecha_fin;
            $inscripcion->importe = $request->importe;
            date_default_timezone_set('America/La_Paz');
            $hoy = date('Y-m-d');
            if($request->fecha_ini > $hoy)
                $inscripcion->estado = 'pendiente';
            if($request->fecha_ini == $hoy)
                $inscripcion->estado = 'activo';
            $inscripcion->save();
        }
        if($request->search != null && isset($request->clientes_id)){
            $cliente = Cliente::find($request->clientes_id);
            $inscripcion = new Inscripcion();
            $inscripcion->clientes_id = $cliente->id;
            $inscripcion->usuarios_id = auth()->user()->id;
            $inscripcion->horarios_disc_id = $request->horarios_disc_id;
            $inscripcion->fecha_ini = $request->fecha_ini2;
            $inscripcion->fecha_fin = $request->fecha_fin2;
            $inscripcion->importe = $request->importe2;
            date_default_timezone_set('America/La_Paz');
            $hoy = date('Y-m-d');
            if($request->fecha_ini2 > $hoy)
                $inscripcion->estado = 'pendiente';
            if($request->fecha_ini2 == $hoy)
                $inscripcion->estado = 'activo';
            $inscripcion->save();
        }
        Session::flash('success-message','datos registrados correctamente !');
        return redirect()->route('inscripciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inscripcion = Inscripcion::getInscripcionById($id);
        $disciplinas = Disciplina::all();
        $horarios = HorarioDisc::getAllHorariosForEdit();
        //dd($horarios);
        return view('inscripciones.edit',['inscripcion'=>$inscripcion, 'horarios'=>$horarios,'disciplinas'=>$disciplinas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $cliente = Cliente::find($request->clientes_id);
        $cliente->nombre_c = $request->nombre_c;
        $cliente->apellido_c = $request->apellido_c;
        $cliente->celular_c = $request->celular_c;
        $cliente->genero = $request->genero;
        $cliente->save();
        $inscripcion = Inscripcion::find($id);
        $inscripcion->clientes_id = $cliente->id;
        $inscripcion->usuarios_id = $request->usuarios_id;
        $inscripcion->horarios_disc_id = $request->horarios_disc_id;
        $inscripcion->fecha_ini = $request->fecha_ini;
        $inscripcion->fecha_fin = $request->fecha_fin;
        $inscripcion->importe = $request->importe;
        date_default_timezone_set('America/La_Paz');
        $hoy = date('Y-m-d');
        if($request->fecha_ini > $hoy)
            $inscripcion->estado = 'pendiente';
        if($request->fecha_ini == $hoy)
            $inscripcion->estado = 'activo';
        $inscripcion->save();
        Session::flash('success-message','datos actualizados correctamente !');
        return redirect()->route('inscripciones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inscripcion = Inscripcion::find($id);
        $inscripcion->delete();
        Session::flash('success-message','datos eliminados correctamente !');
        return redirect()->route('inscripciones.index');
    }
}
