<?php

namespace GymAdmin\Http\Controllers;

use Illuminate\Http\Request;
use GymAdmin\Usuario;
use Session;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $usuarios = Usuario::paginate(3);
        $coleccion = collect($usuarios->all());
        if($coleccion->count() <= 0)
            Session::flash('errors-message','no se encontraron registros !');
        return view('usuarios.index',['usuarios'=>$usuarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = Usuario::create($request->all());
        Session::flash('success-message','usuario registrado correctamente !');
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuario::find($id);
        return view('usuarios.edit',['usuario'=>$usuario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = Usuario::find($id);
        $usuario->nombre_u = $request->nombre_u;
        $usuario->apellido_u = $request->apellido_u;
        $usuario->celular_u = $request->celular_u;
        if(isset($request->username) && isset($request->password)){
            $usuario->username = $request->username;
            $usuario->password = bcrypt($request->password);
        }
        $usuario->save();
        Session::flash('success-message','datos actualizados correctamente !');
        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::find($id);
        $usuario->delete();
        Session::flash('success-message','datos eliminados correctamente !');
        return redirect()->route('usuarios.index');
    }
}