<?php

namespace GymAdmin\Http\Controllers\Auth;

use GymAdmin\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Redirect;
use Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function showLoginForm(){
        if(Auth::check()){
            return redirect()->route('clientes.index');
        }else return view('auth.login');
    }

    public function login(){
        $credenciales = $this->validate(request(),[
            'username'=>'required',
            'password'=>'required'
        ]);
        if(Auth::attempt($credenciales)){
            return redirect()->route('clientes.index');
        }
        return back()->withErrors(['username'=>'Error ! username o password invalidos !'])
                     ->withInput(request(['username']));
    }
    // public function authenticated(){
    //     return redirect()->route('clientes.index');
    // }

    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }

    public function unauthorized(){
        return view('usuarios.unauthorized');
    }
}
