<?php

namespace GymAdmin\Http\Controllers;

use Illuminate\Http\Request;
use GymAdmin\Disciplina;
use Session;

class DisciplinasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disciplinas = Disciplina::paginate(10);
        $coleccion = collect($disciplinas->all());
        if($coleccion->count() <= 0)
            Session::flash('errors-message','no se encontraron registros !');
        return view('disciplinas.index',['disciplinas'=>$disciplinas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('disciplinas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $disciplina = Disciplina::create($request->all());
        Session::flash('success-message','disciplina registrado correctamente !');
        return redirect()->route('disciplinas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $disciplina = Disciplina::find($id);
        return view('disciplinas.edit',['disciplina'=>$disciplina]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $disciplina = Disciplina::find($id);
        $disciplina->nombre_d = $request->nombre_d;
        $disciplina->descripcion = $request->descripcion;
        $disciplina->save();
        Session::flash('success-message','datos actualizados correctamente !');
        return redirect()->route('disciplinas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $disciplina = Disciplina::find($id);
        $disciplina->delete();
        Session::flash('success-message','datos eliminados correctamente !');
        return redirect()->route('disciplinas.index');
    }
}