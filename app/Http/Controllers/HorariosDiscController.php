<?php

namespace GymAdmin\Http\Controllers;

use Illuminate\Http\Request;
use GymAdmin\Disciplina;
use GymAdmin\Instructor;
use GymAdmin\HorarioDisc;
use Session;

class HorariosDiscController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHorariosByDiscId(Request $request, $disciplinas_id){
        if($request->ajax()){
            $horarios = HorarioDisc::getHorariosByDiscId($disciplinas_id);
            return response()->json($horarios);
        }else{
            $horarios = HorarioDisc::getHorariosByDiscId($disciplinas_id);
            return $horarios;
        }
    }
    public function index()
    {
        $horarios = HorarioDisc::getAllHorarios();
        $coleccion = collect($horarios->all());
        if($coleccion->count() <= 0)
            Session::flash('errors-message','no se encontraron resultados !');
        return view('horarios-disciplina.index',['horarios'=>$horarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disciplinas = Disciplina::pluck('nombre_d','id');
        $instructores = Instructor::pluck('nombre_i','id');
        return view('horarios-disciplina.create',['disciplinas'=>$disciplinas,'instructores'=>$instructores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $horarios = HorarioDisc::create($request->all());
        Session::flash('success-message','datos registrados correctamente !');
        return redirect()->route('horarios-disciplina.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $horario = HorarioDisc::find($id);
        $disciplinas = Disciplina::pluck('nombre_d','id');
        $instructores = Instructor::selectRaw('id, CONCAT(nombre_i," ",apellido_i) as full_nombre')->where('instructores.disciplinas_id','=',$horario->disciplinas_id)->pluck('full_nombre','id');
        return view('horarios-disciplina.edit',['horario'=>$horario,'disciplinas'=>$disciplinas,'instructores'=>$instructores]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $horario = HorarioDisc::find($id);
        $horario->disciplinas_id = $request->disciplinas_id;
        $horario->instructores_id = $request->instructores_id;
        $horario->hr_ini = $request->hr_ini;
        $horario->hr_fin = $request->hr_fin;
        $horario->save();
        Session::flash('success-message','datos actualizados correctamente !');
        return redirect()->route('horarios-disciplina.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horario = HorarioDisc::find($id);
        $horario->delete();
        Session::flash('success-message','datos eliminados correctamente !');
        return redirect()->route('horarios-disciplina.index');
    }
}
