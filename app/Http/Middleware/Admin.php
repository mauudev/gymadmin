<?php

namespace GymAdmin\Http\Middleware;

use Closure;
use Auth;
use Session;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->type != 'admin'){
                 Session::flash('unauthorized','Usted no tiene los privilegios suficientes para realizar esta operaci&oacute;n.');
                return redirect()->to('unauthorized');
            }
            return $next($request);
        }else{
            Session::flash('unauthorized-login','Debe iniciar sesi&oacute;n para continuar');
            return redirect()->to('login');
        }
    }
}
