<?php

namespace GymAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class HorarioDisc extends Model
{
    protected $table = 'horarios_disc';
    protected $fillable = ['disciplinas_id',
              					   'instructores_id',
              					   'hr_ini',
                           'hr_fin'];

    public static function getAllHorarios(){
        return DB::table('horarios_disc')
            ->select('horarios_disc.id','horarios_disc.hr_ini','horarios_disc.hr_fin',
                     'instructores.nombre_i','instructores.apellido_i','disciplinas.nombre_d')
            ->join('instructores','instructores.id','=','horarios_disc.instructores_id','left outer')
            ->join('disciplinas','disciplinas.id','=','horarios_disc.disciplinas_id','left outer')
            ->orderBy('horarios_disc.updated_at','desc')
            ->paginate(15);
    }
    public static function getAllHorariosForEdit(){
        return DB::table('horarios_disc')
            ->select('horarios_disc.id','horarios_disc.hr_ini','horarios_disc.hr_fin',
                     'instructores.nombre_i','instructores.apellido_i','disciplinas.nombre_d')
            ->join('instructores','instructores.id','=','horarios_disc.instructores_id','left outer')
            ->join('disciplinas','disciplinas.id','=','horarios_disc.disciplinas_id','left outer')
            ->orderBy('instructores.nombre_i','asc')
            ->get();
    }

    public static function getAllHorariosCreateForm(){
        return DB::table('horarios_disc')
            ->select('horarios_disc.id','horarios_disc.hr_ini','horarios_disc.hr_fin',
                     'instructores.nombre_i','instructores.apellido_i','disciplinas.nombre_d')
            ->join('instructores','instructores.id','=','horarios_disc.instructores_id','left outer')
            ->join('disciplinas','disciplinas.id','=','horarios_disc.disciplinas_id','left outer')
            ->orderBy('horarios_disc.updated_at','desc')
            ->get();
    }
    public static function getAllHorariosExcept($id){
        return DB::table('horarios_disc')
            ->select('horarios_disc.id','horarios_disc.hr_ini','horarios_disc.hr_fin',
                     'instructores.nombre_i','instructores.apellido_i','disciplinas.nombre_d')
            ->join('instructores','instructores.id','=','horarios_disc.instructores_id','left outer')
            ->join('disciplinas','disciplinas.id','=','horarios_disc.disciplinas_id','left outer')
            ->where('horarios_disc.id','<>',$id)
            ->orderBy('horarios_disc.updated_at','desc')
            ->get();
    }
    public static function getHorariosByDiscId($id){
        return DB::table('horarios_disc')
            ->select('horarios_disc.id','horarios_disc.hr_ini','horarios_disc.hr_fin',
                     'instructores.nombre_i','instructores.apellido_i','disciplinas.nombre_d')
            ->join('instructores','instructores.id','=','horarios_disc.instructores_id','left outer')
            ->join('disciplinas','disciplinas.id','=','horarios_disc.disciplinas_id','left outer')
            ->where('disciplinas.id','=',$id)
            ->orderBy('horarios_disc.updated_at','desc')
            ->get();
    }
    
}
