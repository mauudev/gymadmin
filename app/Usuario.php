<?php

namespace GymAdmin;
use Illuminate\Foundation\Auth\User as Authenticatable;//esto para login
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Usuario extends Authenticatable//esto para login
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'usuarios';
    protected $fillable = ['nombre_u',
              					   'apellido_u',
              					   'celular_u',
              					   'username',
              					   'password',
                           'deleted_at'];
    protected $hidden = [
            'password', 'remember_token',
        ];
    public function setNombreUAttribute($value){
    	 $this->attributes['nombre_u'] = ucwords($value);
    } 
    public function setApellidoUAttribute($value){
    	 $this->attributes['apellido_u'] = ucwords($value);
    } 
    public function setPasswordAttribute($value){
       $this->attributes['password'] = bcrypt($value);
    }
}
