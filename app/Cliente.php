<?php

namespace GymAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Cliente extends Model
{
    use SoftDeletes;
    protected $table = 'clientes';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre_c',
              					   'apellido_c',
              					   'celular_c',
                           'genero',
                           'deleted_at'];

    public function setNombreCAttribute($value){
    	 $this->attributes['nombre_c'] = ucwords($value);
    } 
    public function setApellidoCAttribute($value){
    	 $this->attributes['apellido_c'] = ucwords($value);
    } 

    public static function searchClientes($search){
        if("" != trim($search)){
          $target = trim($search);
          return Cliente::select('clientes.id','clientes.nombre_c','clientes.apellido_c',
                          'clientes.celular_c','clientes.genero','clientes.deleted_at','clientes.created_at')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("clientes.nombre_c", "rlike", $search)
                          ->orWhere("clientes.apellido_c","rlike",$search)
                          ->orWhere("clientes.celular_c","rlike",$search)
                          ->orWhere("clientes.genero","rlike",$search)
                          ->orWhere(DB::raw('CONCAT(clientes.nombre_c," ",clientes.apellido_c)'),"rlike",$search);
                    })
                 ->orderBy('clientes.created_at','desc')->get();
        } 
    }
}
