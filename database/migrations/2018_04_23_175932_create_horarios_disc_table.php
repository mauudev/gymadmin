<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariosDiscTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios_disc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('disciplinas_id')->unsigned();
            $table->foreign('disciplinas_id')->references('id')->on('disciplinas')->onDelete('cascade');
            $table->integer('instructores_id')->unsigned();
            $table->foreign('instructores_id')->references('id')->on('instructores')->onDelete('cascade');
            $table->time('hr_ini')->nullable();
            $table->time('hr_fin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios_disc');
    }
}
