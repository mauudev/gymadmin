<?php

use Illuminate\Database\Seeder;

class HorariosDiscTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('horarios_disc')->delete();
        
        \DB::table('horarios_disc')->insert(array (
            0 => 
            array (
                'id' => 1,
                'disciplinas_id' => 1,
                'instructores_id' => 1,
                'hr_ini' => '00:00:00',
                'hr_fin' => '00:00:00',
                'created_at' => '2018-05-03 09:16:27',
                'updated_at' => '2018-05-03 09:16:27',
            ),
            1 => 
            array (
                'id' => 2,
                'disciplinas_id' => 2,
                'instructores_id' => 3,
                'hr_ini' => '21:00:00',
                'hr_fin' => '22:00:00',
                'created_at' => '2018-05-03 09:16:50',
                'updated_at' => '2018-05-03 09:16:50',
            ),
            2 => 
            array (
                'id' => 3,
                'disciplinas_id' => 3,
                'instructores_id' => 2,
                'hr_ini' => '19:00:00',
                'hr_fin' => '20:00:00',
                'created_at' => '2018-05-03 09:17:07',
                'updated_at' => '2018-05-03 09:17:07',
            ),
        ));
        
        
    }
}