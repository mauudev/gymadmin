<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClientesTableSeeder::class);
        $this->call(UsuariosTableSeeder::class);
        $this->call(DisciplinasTableSeeder::class);
        $this->call(InstructoresTableSeeder::class);
        $this->call(HorariosDiscTableSeeder::class);
    }
}
