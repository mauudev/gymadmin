<?php

use Illuminate\Database\Seeder;

class InstructoresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('instructores')->delete();
        
        \DB::table('instructores')->insert(array (
            0 => 
            array (
                'id' => 1,
                'disciplinas_id' => 1,
                'nombre_i' => 'Carlos',
                'apellido_i' => 'Rojas Velarde',
                'celular_i' => '877373889',
                'created_at' => '2018-04-24 06:30:57',
                'updated_at' => '2018-04-24 06:30:57',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'disciplinas_id' => 3,
                'nombre_i' => 'Gonzalo',
                'apellido_i' => 'Valverde',
                'celular_i' => '384384389',
                'created_at' => '2018-04-24 06:31:12',
                'updated_at' => '2018-04-24 06:31:12',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'disciplinas_id' => 2,
                'nombre_i' => 'Daniela',
                'apellido_i' => 'Claure',
                'celular_i' => '83773738778',
                'created_at' => '2018-04-24 06:31:25',
                'updated_at' => '2018-04-24 06:31:25',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}