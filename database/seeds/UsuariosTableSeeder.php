<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('usuarios')->delete();
        
        \DB::table('usuarios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_u' => 'Mauricio',
                'apellido_u' => 'Trigo Uriona',
                'celular_u' => '79933310',
                'username' => 'maudev143',
                'password' => '$2y$10$6PeTibCs.sMfNdEnGGryJ.YvBHkbATNG97Oo1n3DWQ08sNIchUgF.',
                'type' => 'normal',
                'remember_token' => 'dQRdDr5c2p1z8ZTKqHF8tR4YL25QKdIjvNIIdzhP1MVrI7i5LZA8FjP35yyP',
                'created_at' => '2018-05-03 04:30:07',
                'updated_at' => '2018-05-03 04:30:07',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}