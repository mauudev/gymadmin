<?php

use Illuminate\Database\Seeder;

class DisciplinasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('disciplinas')->delete();
        
        \DB::table('disciplinas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_d' => 'Aparatos',
                'descripcion' => 'Aparatos sin horarios',
                'created_at' => '2018-04-24 06:29:03',
                'updated_at' => '2018-04-24 06:29:03',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre_d' => 'Zumba',
                'descripcion' => 'Bailes seee',
                'created_at' => '2018-04-24 06:29:13',
                'updated_at' => '2018-04-24 06:29:13',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre_d' => 'Spinning',
                'descripcion' => 'Bicicletas full bro',
                'created_at' => '2018-04-24 06:29:29',
                'updated_at' => '2018-04-24 06:29:29',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}