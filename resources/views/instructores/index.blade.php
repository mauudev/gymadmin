@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Instructores</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
			<div class="col-sm-7 d-none d-md-block">
				<a href="{{ URL::to('instructores/create') }}" class="btn btn-primary float-right"><i class="icon-plus"></i> Registrar instructor</a>
			</div>
		</div><br>
		<table class="table table-responsive-sm table-striped">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Celular</th>
					<th>Disciplina</th>
					<th>Acci&oacute;n</th>
				</tr>
			</thead>
			<tbody>
				@foreach($instructores as $instructor)
				<tr>
					<td>{{ $instructor->nombre_i }}</td>
					<td>{{ $instructor->apellido_i }}</td>
					<td>{{ $instructor->celular_i }}</td>
					<td>{{ $instructor->nombre_d }}</td>
					<td>
						<div class="btn-group" role="group" aria-label="Basic example">
						<a href="{{ URL::to('instructores/'.$instructor->id) }}" data-balloon="Ver" data-balloon-pos="up" class="btn btn-primary"><i class="fa fa-eye"></i></a>
						<a href="{{ URL::to('instructores/'.$instructor->id.'/edit') }}" data-balloon="Editar" data-balloon-pos="up" class="btn btn-warning"><i class="fa fa-edit"></i></a>
						<a href="{{ URL::to('instructores/'.$instructor->id.'/destroy') }}" data-balloon="Eliminar" data-balloon-pos="up" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>	
		
		@include('alerts.success')
		@include('alerts.errors')
	</div>
</div>
{{ $instructores->links('pagination::bootstrap-4') }}
@endsection