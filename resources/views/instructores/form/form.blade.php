<div class="row instructorMainForm1">
	<div class="col-md-5">
		<div class="form-group"> 
			{{ Form::label('nombre_i','Nombres') }}
			{{ Form::text('nombre_i',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del instructor','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group"> 
			{{ Form::label('apellido_i','Apellidos') }}
			{{ Form::text('apellido_i',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del instructor','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group"> 
			{{ Form::label('celular_i','Celular') }}
			{{ Form::text('celular_i',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del instructor','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group"> 
			{{ Form::label('disciplinas_id','Disciplina') }}
			{{ Form::select('disciplinas_id',$disciplinas, null,['class'=>'form-control','placeholder'=>'Seleccione una disciplina..','required']) }}
		</div>
	</div>
</div>