@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Registro de instructores</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
	{!! Form::open(['route'=>'instructores.store','method'=>'POST','class'=>'form-group']) !!}	
	@include('instructores.form.form')
	@include('alerts.success')
	<button type="button" class="btn btn-link agregarHorarioForm1"><i class="fa fa-plus"></i>&nbsp; Asignar horario para este instructor</button><br class="spaceJump1"><br class="spaceJump1">
	<div class="alert alert-info" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    		<span aria-hidden="true">&times;</span>
    	</button>
    	<strong><i class="fa fa-info-circle"></i> NOTA:</strong> Si no asigna un horario para este instructor ahora, deber&aacute; hacerlo manualmente en la secci&oacute;n <b>'Horarios disciplina'</b> y el instructor registrado <b>no figurar&aacute; al momento de registrar una nueva inscripci&oacute;n.</b> 
    </div>
	<div class="card-footer">
	{!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default" href="{!! URL::to('instructores') !!}">Cancelar</a>
	</div>
</div>
@endsection