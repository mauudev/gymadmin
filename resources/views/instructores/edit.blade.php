@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Editar datos del instructor</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
	{!!Form::model($instructor,['route'=>['instructores.update',$instructor->id],'method'=>'PUT'])!!}	
	<div class="row">
		<div class="col-md-4">
			<div class="form-group"> 
				{{ Form::label('nombre_i','Nombre') }}
				{{ Form::text('nombre_i',$instructor->nombre_i,['class'=>'form-control','placeholder'=>'Ingrese el nombre del instructor','required','min'=>5]) }}
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group"> 
				{{ Form::label('apellido_i','Apellidos') }}
				{{ Form::text('apellido_i',$instructor->apellido_i,['class'=>'form-control','placeholder'=>'Ingrese el apellido del instructor','required','min'=>5]) }}
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group"> 
				{{ Form::label('celular_i','Celular') }}
				{{ Form::text('celular_i',$instructor->celular_i,['class'=>'form-control','placeholder'=>'Ingrese el celular del instructor','required','min'=>5]) }}
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group"> 
				{{ Form::label('disciplinas_id','Celular') }}
				{{ Form::select('disciplinas_id',$disciplinas,null,['class'=>'form-control','placeholder'=>'Seleccione una disciplina..','required']) }}
			</div>
		</div>
	</div>
	@include('alerts.success')
	</div>
	<div class="card-footer">
	{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!} <a class="btn 	btn-default" href="{!! URL::to('instructores') !!}">Cancelar</a>
	</div>
</div>
@endsection