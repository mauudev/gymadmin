<br><h5>Resultados de la b&uacute;squeda</h5><hr>
<table class="table table-responsive-sm table-striped searchTable1">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Celular</th>
			<th>Selecci&oacute;n</th>
		</tr>
	</thead>
<tbody class="resultadosTable">
	@foreach($clientes as $cliente)
		<tr>
			<td>{{ $cliente->nombre_c }}</td>
			<td>{{ $cliente->apellidos_c }}</td>
			<td>{{ $cliente->celular_c }}</td>
			<td align="left">
				<input name="clientes_id" type="radio" value="{{ $clientes->id }}">
			</td>
		</tr>
	@endforeach
</tbody>
</table>
