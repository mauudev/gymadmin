<div class="row">
	<div class="col-md-12">
		<h5>Datos del cliente</h5><hr>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('nombre_c','Nombre') }}
			{{ Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('apellido_c','Apellidos') }}
			{{ Form::text('apellido_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('celular_c','Celular') }}
			{{ Form::text('celular_c',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-3">
		{{ Form::label('genero','G&eacute;nero') }}<br>
		<div class="btn-group radio-group">
			<label class="btn btn-primary"><i class="fa fa-mars"></i> Masculino
				<input type="radio" value="masculino" name="genero" checked="">
			</label>
			<label class="btn btn-primary not-active"><i class="fa fa-venus"></i> Femenino
				<input type="radio" value="femenino" name="genero">
			</label>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('fecha_ini','Fecha inicio') }}
			{{ Form::text('fecha_ini',null,['class'=>'form-control','id'=>'fecha_ini1','placeholder'=>'Fecha de inicio de la inscripci&oacute;n']) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('fecha_fin','Fecha fin') }}
			{{ Form::text('fecha_fin',null,['class'=>'form-control','id'=>'fecha_fin1','placeholder'=>'Fecha de fin de la inscripci&oacute;n']) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('importe','Importe') }}
			{{ Form::text('importe',null,['class'=>'form-control','placeholder'=>'Ingrese el monto']) }}
		</div>
	</div>
	<div class="col-md-12">
		<br><h5>Disciplina, instructor y horario</h5><hr>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			{{ Form::label('disciplina','Disciplina') }}
			{{ Form::select('disciplina',$disciplinas,null,['class'=>'form-control disciplinaSelectInscripcion','placeholder'=>'Seleccione una disciplina..']) }}
		</div>
	</div>
	<div class="col-md-12">
		<table class="table table-responsive-sm table-striped">
			<thead>
				<tr>
					<th>Instructor</th>
					<th>Hora inicio</th>
					<th>Hora fin</th>
					<th>Selecci&oacute;n</th>
				</tr>
			</thead>
			<tbody class="instructorTable">
				
			</tbody>
		</table>
		<div class="alert alert-info" role="alert" id="instructorTableInfo">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong><i class="fa fa-info-circle"></i> Info:</strong> Seleccione una disciplina para ver los horarios disponibles.
		</div>
	</div>
</div>