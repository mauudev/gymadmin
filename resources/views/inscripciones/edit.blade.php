@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Editar datos de la inscripci&oacute;n</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
		{!! Form::model($inscripcion,['route'=>['inscripciones.update',$inscripcion->id],'method'=>'PUT']) !!}
		{{ Form::hidden('clientes_id',$inscripcion->clientes_id) }}
		{{ Form::hidden('usuarios_id',$inscripcion->usuarios_id) }}
		{{-- @include('inscripciones.form.form') --}}
		<div class="animated fadeIn">
			<div class="row">
				<div class="col-md-12 mb-4">
					<div class="row">
						<div class="col-md-12">
							<h5>Datos del cliente</h5><hr>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::label('nombre_c','Nombre') }}
								{{ Form::text('nombre_c',$inscripcion->nombre_c,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::label('apellido_c','Apellidos') }}
								{{ Form::text('apellido_c',$inscripcion->apellido_c,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::label('celular_c','Celular') }}
								{{ Form::text('celular_c',$inscripcion->celular_c,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','required','min'=>5]) }}
							</div>
						</div>
						<div class="col-md-3">
							{{ Form::label('genero','G&eacute;nero') }}<br>
							<div class="btn-group radio-group">
								<label class="btn btn-primary {{ $inscripcion->genero == 'masculino' ? '':'not-active' }}">Masculino
									<input type="radio" value="masculino" name="genero" {{ $inscripcion->genero == 'masculino' ? 'checked':'' }}>
								</label>
								<label class="btn btn-primary {{ $inscripcion->genero == 'femenino' ? '':'not-active' }}">Femenino
									<input type="radio" value="femenino" name="genero" {{ $inscripcion->genero == 'femenino' ? 'checked':'' }}>
								</label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::label('fecha_ini','Fecha inicio') }}
								{{ Form::text('fecha_ini',$inscripcion->fecha_ini,['class'=>'form-control','id'=>'fecha_ini1','placeholder'=>'Fecha de inicio de la inscripci&oacute;n','required']) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::label('fecha_fin','Fecha fin') }}
								{{ Form::text('fecha_fin',$inscripcion->fecha_fin,['class'=>'form-control','id'=>'fecha_fin1','placeholder'=>'Fecha de fin de la inscripci&oacute;n','required']) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::label('importe','Importe') }}
								{{ Form::text('importe',$inscripcion->importe,['class'=>'form-control','placeholder'=>'Ingrese el monto','required']) }}
							</div>
						</div>
						<div class="col-md-12">
							<br><h5>Disciplina, instructor y horario</h5><hr>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('disciplina','Disciplina') }}
								<select class="form-control disciplinaSelectInscripcion" id="disciplina" name="disciplina">
									@foreach($disciplinas as $disciplina)
									@if($disciplina->id == $inscripcion->disciplinas_id)
									<option value="{{ $disciplina->id }}" selected >{{ $disciplina->nombre_d }}
									</option>
									@else
									<option value="{{ $disciplina->id }}" >{{ $disciplina->nombre_d }}
									</option>
									@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<table class="table table-responsive-sm table-striped">
								<thead>
										<th>Instructor</th>
										<th>Hora inicio</th>
										<th>Hora fin</th>
										<th>Selecci&oacute;n</th>
									</tr>
								</thead>
								<tbody  class="instructorTable">
									@foreach($horarios as $horario)
									<tr>
										<td>{{ $horario->nombre_i.' '.$horario->apellido_i }}</td>
										<td>{{ $horario->hr_ini }}</td>
										<td>{{ $horario->hr_fin }}</td>
										<td align="left">
											{{ Form::radio('horarios_disc_id',$horario->id) }}
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('alerts.success')
	</div>
	<div class="card-footer">
		{!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default" href="{!! URL::to('inscripciones') !!}">Cancelar</a>
	</div>
</div>
@endsection