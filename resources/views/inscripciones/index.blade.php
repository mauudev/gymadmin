@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Clientes inscritos</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
			<div class="col-sm-7 d-none d-md-block">
				<a href="{{ URL::to('inscripciones/create') }}" class="btn btn-primary float-right"><i class="icon-plus"></i> Registrar inscripci&oacute;n</a>
			</div>
		</div><br>
		<table class="table table-responsive-sm table-striped">
			<thead>
				<tr>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Celular</th>
					<th>Disciplina</th>
					<th>Fecha inicio</th>
					<th>Fecha fin</th>
					<th>Estado</th>
					<th>Acci&oacute;n</th>
				</tr>
			</thead>
			<tbody>
				@foreach($inscripciones as $inscrito)
				<tr>
					<td>{{ $inscrito->nombre_c }}</td>
					<td>{{ $inscrito->apellido_c }}</td>
					<td>{{ $inscrito->celular_c }}</td>
					<td>{{ $inscrito->nombre_d }}</td>
					<td>{{ $inscrito->fecha_ini }}</td>
					<td>{{ $inscrito->fecha_fin }}</td>
					<td>
						@if($inscrito->estado == 'activo')
						<span class="badge badge-success">Activo</span>
						@endif
						@if($inscrito->estado == 'pendiente')
						<span class="badge badge-warning">Pendiente</span>
						@endif
						@if($inscrito->estado == 'caducado')
						<span class="badge badge-danger">Caducado</span>
						@endif
					</td>
					<td>
						<div class="btn-group" role="group" aria-label="Basic example">
						<a href="{{ URL::to('inscripciones/'.$inscrito->id) }}" data-balloon="Ver" data-balloon-pos="up" class="btn btn-primary"><i class="fa fa-eye"></i></a>
						<a href="{{ URL::to('inscripciones/'.$inscrito->id.'/edit') }}" data-balloon="Editar" data-balloon-pos="up" class="btn btn-warning"><i class="fa fa-edit"></i></a>
						<a href="{{ URL::to('inscripciones/'.$inscrito->id.'/destroy') }}" data-balloon="Eliminar" data-balloon-pos="up" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>	
		
		@include('alerts.success')
		@include('alerts.errors')
	</div>
</div>
{{ $inscripciones->links('pagination::bootstrap-4') }}
@endsection