@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Registrar inscripci&oacute;n</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
		{!! Form::open(['route'=>'inscripciones.store','method'=>'POST','class'=>'form-group']) !!}
		{{-- @include('inscripciones.form.form') --}}
		<div class="animated fadeIn">
			<div class="row">
				<div class="col-md-12 mb-4">
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active show" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-search"></i> Buscar cliente</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#profile3" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-plus"></i> Nuevo cliente</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active show" id="home3" role="tabpanel">
							<div class="col-md-5">
								<div class="input-group">
									{!! Form::text("search",null,["class"=>"form-control search1","id"=>"input1-group2" ,"placeholder"=>"Buscar cliente..","required"]) !!}
									<span class="input-group-prepend">
										<button type="button" class="btn btn-primary search-cliente1"><i class="fa fa-search"></i> Buscar</button>
									</span>
								</div>
							</div>
							<div class="col-md-12" id="searchResults1">
								<br><h5>Resultados de la b&uacute;squeda</h5><hr>
								<table class="table table-responsive-sm table-striped">
									<thead>
										<tr>
											<th>Nombre</th>
											<th>Apellidos</th>
											<th>Celular</th>
											<th>Selecci&oacute;n</th>
										</tr>
									</thead>
									<tbody class="resultadosTable"></tbody>
								</table>
								<div class="alert alert-info" role="alert" id="clientesTableInfo">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong><i class="fa fa-info-circle"></i> Info:</strong> Ingrese los datos del cliente y presione en 'Buscar'
								</div>
							</div>
						<div class="col-md-12">
							<br><h5>Disciplina, instructor y horario</h5><hr>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('disciplina','Disciplina') }}
								{{ Form::select('disciplina',$disciplinas,null,['class'=>'form-control disciplinaSelectInscripcion','placeholder'=>'Seleccione una disciplina..','required']) }}
							</div>
						</div>
						<div class="col-md-12">
							<table class="table table-responsive-sm table-striped">
								<thead>
									<tr>
										<th>Instructor</th>
										<th>Hora inicio</th>
										<th>Hora fin</th>
										<th>Selecci&oacute;n</th>
									</tr>
								</thead>
								<tbody class="instructorTable">
									
								</tbody>
							</table>
							<div class="alert alert-info" role="alert" id="instructorTableInfo">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><i class="fa fa-info-circle"></i> Info:</strong> Seleccione una disciplina para ver los horarios disponibles.
							</div>
						</div>
					</div>
					<div class="tab-pane" id="profile3" role="tabpanel">
						@include('inscripciones.form.form')
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('alerts.success')
</div>
<div class="card-footer">
	{!!Form::submit('Guardar',['class'=>'btn btn-primary','type'=>'button'])!!} <a class="btn btn-default" href="{!! URL::to('inscripciones') !!}">Cancelar</a>
</div>
</div>
@endsection