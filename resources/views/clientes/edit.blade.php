@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Editar datos del cliente</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
		{!!Form::model($cliente,['route'=>['clientes.update',$cliente->id],'method'=>'PUT'])!!}
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					{{ Form::label('nombre_c','Nombre') }}
					{{ Form::text('nombre_c',$cliente->nombre_c,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]) }}
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{{ Form::label('apellido_c','Apellidos') }}
					{{ Form::text('apellido_c',$cliente->apellido_c,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]) }}
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{{ Form::label('celular_c','Celular') }}
					{{ Form::text('celular_c',$cliente->celular_c,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','required','min'=>5]) }}
				</div>
			</div>
			<div class="col-md-3">
				{{ Form::label('genero','G&eacute;nero') }}<br>
				<div class="btn-group radio-group">
					<label class="btn btn-primary {{ $cliente->genero == 'masculino' ? '':'not-active' }}">Masculino
						<input type="radio" value="masculino" name="genero" {{ $cliente->genero == 'masculino' ? 'checked':'' }}>
					</label>
					<label class="btn btn-primary {{ $cliente->genero == 'femenino' ? '':'not-active' }}">Femenino
						<input type="radio" value="femenino" name="genero" {{ $cliente->genero == 'femenino' ? 'checked':'' }}>
					</label>
				</div>
			</div>
		</div>
		@include('alerts.success')
	</div>
	<div class="card-footer">
		{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!} <a class="btn 	btn-default" href="{!! URL::to('clientes') !!}">Cancelar</a>
	</div>
</div>
@endsection