<div class="row">
	<div class="col-md-3">
		<div class="form-group"> 
			{{ Form::label('nombre_c','Nombre') }}
			{{ Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group"> 
			{{ Form::label('apellido_c','Apellidos') }}
			{{ Form::text('apellido_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group"> 
			{{ Form::label('celular_c','Celular') }}
			{{ Form::text('celular_c',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-3">
		{{ Form::label('genero','G&eacute;nero') }}<br>
		<div class="btn-group radio-group">
			<label class="btn btn-primary"><i class="fa fa-mars"></i> Masculino
				<input type="radio" value="masculino" name="genero" checked="">
			</label>
			<label class="btn btn-primary not-active"><i class="fa fa-venus"></i> Femenino
				<input type="radio" value="femenino" name="genero">
			</label>
		</div>
	</div>
</div>
