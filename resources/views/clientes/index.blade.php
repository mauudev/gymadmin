@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Clientes</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
			<div class="col-sm-7 d-none d-md-block">
				<a href="{{ URL::to('clientes/create') }}" class="btn btn-primary float-right"><i class="icon-plus"></i> Registrar cliente</a>
			</div>
		</div><br>
		<table class="table table-responsive-sm table-striped">
			<thead>
				<tr>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Celular</th>
					<th>Fecha de registro</th>
					<th>Estado</th>
					<th>Acci&oacute;n</th>
				</tr>
			</thead>
			<tbody>
				@foreach($clientes as $cliente)
				<tr>
					<td>{{ $cliente->nombre_c }}</td>
					<td>{{ $cliente->apellido_c }}</td>
					<td>{{ $cliente->celular_c }}</td>
					<td>{{ $cliente->created_at }}</td>
					<td>
						<span class="badge badge-success">Active</span>
					</td>
					<td>
						<div class="btn-group" role="group" aria-label="Basic example">
						<a href="{{ URL::to('clientes/'.$cliente->id) }}" data-balloon="Ver" data-balloon-pos="up" class="btn btn-primary"><i class="fa fa-eye"></i></a>
						<a href="{{ URL::to('clientes/'.$cliente->id.'/edit') }}" data-balloon="Editar" data-balloon-pos="up" class="btn btn-warning"><i class="fa fa-edit"></i></a>
						<a href="{{ URL::to('clientes/'.$cliente->id.'/destroy') }}" data-balloon="Eliminar" data-balloon-pos="up" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>	
		
		@include('alerts.success')
		@include('alerts.errors')
	</div>
</div>
{{ $clientes->links('pagination::bootstrap-4') }}
@endsection