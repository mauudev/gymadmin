@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Registro de clientes</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
	{!! Form::open(['route'=>'clientes.store','method'=>'POST','class'=>'form-group']) !!}	
	@include('clientes.form.form')
	@include('alerts.success')
	</div>
	<div class="card-footer">
	{!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default" href="{!! URL::to('clientes') !!}">Cancelar</a>
	</div>
</div>
@endsection