@if(Session::has('errors-message'))
    <div class="alert alert-danger" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-ban"></i> Error!</strong> {!! session('errors-message') !!}
    </div>
@endif
<script type="text/javascript">
$(document).ready(function(){
    $("#msg_confirmation1").fadeOut(6000)
});
</script>