
  @if(Session::has('success-message'))
    <div class="alert alert-success" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-check"></i> Exito!</strong> {!! session('success-message') !!}
    </div>
  @elseif(Session::has('toast-success'))
  <script type="text/javascript">
    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    };
    toastr.success('<b>Exito!</b> {!! session("toast-success") !!} ');
  </script>
  @endif
  <script type="text/javascript">
    $(document).ready(function(){
        $("#msg_confirmation1").fadeOut(6000)
    });
  </script>