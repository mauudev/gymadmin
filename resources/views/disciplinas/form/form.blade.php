<div class="row">
	<div class="col-md-4">
		<div class="form-group"> 
			{{ Form::label('nombre_d','Nombre de la disciplina') }}
			{{ Form::text('nombre_d',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre de la disciplina','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group"> 
			{{ Form::label('descripcion','Descripcion') }}
			{{ Form::textarea('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese una pequena descripcion de lo que es la disciplina','required','min'=>5,'rows'=>3]) }}
		</div>
	</div>
</div>