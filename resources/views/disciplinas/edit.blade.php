@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Editar datos de la disciplina</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
	{!!Form::model($disciplina,['route'=>['disciplinas.update',$disciplina->id],'method'=>'PUT'])!!}	
	<div class="row">
		<div class="col-md-4">
			<div class="form-group"> 
				{{ Form::label('nombre_d','Nombre de la disciplina') }}
				{{ Form::text('nombre_d',$disciplina->nombre_d,['class'=>'form-control','placeholder'=>'Ingrese el nombre del disciplina','required','min'=>5]) }}
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group"> 
				{{ Form::label('descripcion','Descripcion') }}
				{{ Form::textarea('descripcion',$disciplina->descripcion,['class'=>'form-control','placeholder'=>'Ingrese una pequena descripcion de lo que es la disciplina','required','min'=>5,'rows'=>3]) }}
			</div>
		</div>
	</div>
	@include('alerts.success')
	</div>
	<div class="card-footer">
	{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!} <a class="btn 	btn-default" href="{!! URL::to('disciplinas') !!}">Cancelar</a>
	</div>
</div>
@endsection