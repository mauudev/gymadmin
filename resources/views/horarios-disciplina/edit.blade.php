@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Editar datos del horario</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
	{!!Form::model($horario,['route'=>['horarios-disciplina.update',$horario->id],'method'=>'PUT'])!!}	
	<div class="row">
	<div class="col-md-5">
		<div class="form-group">
			{{ Form::label('disciplinas_id','Disciplina') }}
			{{ Form::select('disciplinas_id',$disciplinas,null,['class'=>'form-control disciplinaSelect','placeholder'=>'Seleccione una disciplina..','required']) }}
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			{{ Form::label('instructores_id','Instructor') }}
			{{ Form::select('instructores_id',$instructores,null,['class'=>'form-control instructorSelect','placeholder'=>'Seleccione un instructor..','required']) }}
		</div>
	</div>
	<div class="col-md-5">
		{{ Form::label('hr_ini','Hora de inicio') }}
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"><i class="fa fa-clock-o"></i></span>
			</div>
			{{ Form::text('hr_ini',$horario->hr_ini,['class'=>'form-control','id'=>'hr_ini1','placeholder'=>'Haga click aqu&iacute; para elegir una hora']) }}
		</div>
	</div>
	<div class="col-md-5">
		{{ Form::label('hr_fin','Hora de finalizaci&oacute;n') }}
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"><i class="fa fa-clock-o"></i></span>
			</div>
			{{ Form::text('hr_fin',$horario->hr_fin,['class'=>'form-control','id'=>'hr_fin1','placeholder'=>'Haga click aqu&iacute; para elegir una hora']) }}
		</div>
	</div>
</div>
<script type="text/javascript">
$('#hr_ini1').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
$('#hr_fin1').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
</script>
	@include('alerts.success')
	</div>
	<div class="card-footer">
	{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!} <a class="btn 	btn-default" href="{!! URL::to('disciplinas') !!}">Cancelar</a>
	</div>
</div>
@endsection