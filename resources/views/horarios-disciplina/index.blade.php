@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Horarios por disciplina</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
			<div class="col-sm-7 d-none d-md-block">
				<a href="{{ URL::to('horarios-disciplina/create') }}" class="btn btn-primary float-right"><i class="icon-plus"></i> Registrar horario</a>
			</div>
		</div><br>
		<table class="table table-responsive-sm table-striped">
			<thead>
				<tr>
					<th>Disciplina</th>
					<th>Instructor</th>
					<th>Hora inicio</th>
					<th>Hora fin</th>
					<th>Acci&oacute;n</th>
				</tr>
			</thead>
			<tbody>
				@foreach($horarios as $horario)
				<tr>
					<td>{{ $horario->nombre_d }}</td>
					<td>{{ $horario->nombre_i.' '.$horario->apellido_i }}</td>
					<td>{{ $horario->hr_ini }}</td>
					<td>{{ $horario->hr_fin }}</td>
					<td>
						<div class="btn-group" role="group" aria-label="Basic example">
						<a href="{{ URL::to('horarios-disciplina/'.$horario->id) }}" data-balloon="Ver" data-balloon-pos="up" class="btn btn-primary"><i class="fa fa-eye"></i></a>
						<a href="{{ URL::to('horarios-disciplina/'.$horario->id.'/edit') }}" data-balloon="Editar" data-balloon-pos="up" class="btn btn-warning"><i class="fa fa-edit"></i></a>
						<a href="{{ URL::to('horarios-disciplina/'.$horario->id.'/destroy') }}" data-balloon="Eliminar" data-balloon-pos="up" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>		
		@include('alerts.success')
		@include('alerts.errors')
	</div>
</div>
{{ $horarios->links('pagination::bootstrap-4') }}
@endsection