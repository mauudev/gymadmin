@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Registro de horarios</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
		</div><br>
	{!! Form::open(['route'=>'horarios-disciplina.store','method'=>'POST','class'=>'form-group']) !!}	
	@include('horarios-disciplina.form.form')
	@include('alerts.success')
	</div>
	<div class="card-footer">
	{!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default" href="{!! URL::to('horarios-disciplina') !!}">Cancelar</a>
	</div>
</div>
@endsection