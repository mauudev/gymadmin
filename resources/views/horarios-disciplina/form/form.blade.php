<div class="row">
	<div class="col-md-5">
		<div class="form-group">
			{{ Form::label('disciplinas_id','Disciplina') }}
			{{ Form::select('disciplinas_id',$disciplinas,null,['class'=>'form-control disciplinaSelect','placeholder'=>'Seleccione una disciplina..','required']) }}
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			{{ Form::label('instructores_id','Instructor') }}
			{{ Form::select('instructores_id',array(),null,['class'=>'form-control instructorSelect','placeholder'=>'Seleccione un instructor..','required']) }}
		</div>
	</div>
	<div class="col-md-5">
		{{ Form::label('hr_ini','Hora de inicio') }}
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"><i class="fa fa-clock-o"></i></span>
			</div>
			<input type="text" id="hr_ini1" name="hr_ini" class="form-control" placeholder="Haga click aqu&iacute; para elegir una hora">
		</div>
	</div>
	<div class="col-md-5">
		{{ Form::label('hr_fin','Hora de finalizaci&oacute;n') }}
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"><i class="fa fa-clock-o"></i></span>
			</div>
			<input type="text" id="hr_fin1" name="hr_fin" class="form-control" placeholder="Haga click aqu&iacute; para elegir una hora">
		</div>
	</div>
</div>
