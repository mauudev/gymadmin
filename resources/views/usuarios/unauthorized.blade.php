<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from coreui.io/demo/Static_Demo/pages-500.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Apr 2018 00:24:22 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="CoreUI Bootstrap 4 Admin Template">
        <meta name="author" content="Lukasz Holeczek">
        <meta name="keyword" content="CoreUI Bootstrap 4 Admin Template">
        <title>No autorizado !</title>
        {{ Html::style('css/bootstrap.min.css') }}
        {{ Html::style('vendors/css/flag-icon.min.css') }}
        {{ Html::style('vendors/css/font-awesome.min.css') }}
        {{ Html::style('vendors/css/simple-line-icons.min.css') }}
        {{ Html::style('css/style.css') }}
        {{ Html::style('css/balloon.min.css') }}
        {{ Html::style('css/bootstrap-material-datetimepicker.css') }}

        {{ Html::script('vendors/js/jquery.min.js') }}
        {{ Html::script('vendors/js/bootstrap.min.js') }}
        {{ Html::script('vendors/js/popper.min.js') }}
        {{ Html::script('vendors/js/pace.min.js') }}
        {{ Html::script('vendors/js/Chart.min.js') }}
        {{ Html::script('js/moment.js') }}
        {{ Html::script('js/bootstrap-material-datetimepicker.js') }}
        <!-- JQuery & Ajax queries-->
        {{ Html::script('js/fieldcontents.js') }}
    </head>
    <body class="app flex-row align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="clearfix">
                        <h1 class="float-left display-3 mr-4">403</h1>
                        <h4 class="pt-3">Houston, tenemos un problema!</h4>
                        <p class="text-muted">No tienes permiso para acceder a esta secci&oacute;n.</p>
                    </div>
                    <div class="input-prepend input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                        </div>
                        <input id="prependedInput" class="form-control" size="16" type="text" placeholder="What are you looking for?">
                        <span class="input-group-append">
                            <button class="btn btn-info" type="button">Search</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <!-- Mirrored from coreui.io/demo/Static_Demo/pages-500.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Apr 2018 00:24:22 GMT -->
</html>