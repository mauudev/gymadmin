@extends('layouts.main')
@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">Usuarios</h4>
				<div class="small text-muted">Administraci&oacute;n</div>
			</div>
			<div class="col-sm-7 d-none d-md-block">
				<a href="{{ URL::to('usuarios/create') }}" class="btn btn-primary float-right"><i class="icon-plus"></i> Registrar usuario</a>
			</div>
		</div><br>
		<table class="table table-responsive-sm table-striped">
			<thead>
				<tr>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Celular</th>
					<th>Username</th>
					<th>Estado</th>
					<th>Acci&oacute;n</th>
				</tr>
			</thead>
			<tbody>
				@foreach($usuarios as $usuario)
				<tr>
					<td>{{ $usuario->nombre_u }}</td>
					<td>{{ $usuario->apellido_u }}</td>
					<td>{{ $usuario->celular_u }}</td>
					<td>{{ $usuario->username }}</td>
					<td>
						<span class="badge badge-success">Active</span>
					</td>
					<td>
						<div class="btn-group" role="group" aria-label="Basic example">
						<a href="{{ URL::to('usuarios/'.$usuario->id) }}" data-balloon="Ver" data-balloon-pos="up" class="btn btn-primary"><i class="fa fa-eye"></i></a>
						<a href="{{ URL::to('usuarios/'.$usuario->id.'/edit') }}" data-balloon="Editar" data-balloon-pos="up" class="btn btn-warning"><i class="fa fa-edit"></i></a>
						<a href="{{ URL::to('usuarios/'.$usuario->id.'/destroy') }}" data-balloon="Eliminar" data-balloon-pos="up" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>	
		
		@include('alerts.success')
		@include('alerts.errors')
	</div>
</div>
{{ $usuarios->links('pagination::bootstrap-4') }}
@endsection