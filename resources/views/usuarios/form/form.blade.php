<div class="row">
	<div class="col-md-4">
		<div class="form-group"> 
			{{ Form::label('nombre_u','Nombre') }}
			{{ Form::text('nombre_u',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group"> 
			{{ Form::label('apellido_u','Apellidos') }}
			{{ Form::text('apellido_u',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group"> 
			{{ Form::label('celular_u','Celular') }}
			{{ Form::text('celular_u',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group"> 
			{{ Form::label('type','Tipo usuario') }}
			{{ Form::select('type',['admin'=>'Administrador','normal'=>'Usuario normal'],null,['class'=>'form-control','placeholder'=>'Seleccione una opci&oacute;n..','required']) }}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group"> 
			{{ Form::label('username','Username') }}
			{{ Form::text('username',null,['class'=>'form-control','placeholder'=>'Ingrese el username','required','min'=>5]) }}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group"> 
			{{ Form::label('password','Password') }}
			{{ Form::password('password',['class'=>'form-control','placeholder'=>'Ingrese el password','required','min'=>5]) }}
		</div>
	</div>
</div>