<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from coreui.io/demo/Static_Demo/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Apr 2018 00:24:22 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="CoreUI Bootstrap 4 Admin Template">
        <meta name="author" content="Lukasz Holeczek">
        <meta name="keyword" content="CoreUI Bootstrap 4 Admin Template">
        <title>Login</title>
        {{ Html::style('css/bootstrap.min.css') }}
        {{ Html::style('vendors/css/flag-icon.min.css') }}
        {{ Html::style('vendors/css/font-awesome.min.css') }}
        {{ Html::style('vendors/css/simple-line-icons.min.css') }}
        {{ Html::style('css/style.css') }}
        {{ Html::style('css/balloon.min.css') }}
        {{ Html::style('css/bootstrap-material-datetimepicker.css') }}
        {{ Html::script('vendors/js/jquery.min.js') }}
        {{ Html::script('vendors/js/bootstrap.min.js') }}
        {{ Html::script('vendors/js/popper.min.js') }}
        {{ Html::script('vendors/js/pace.min.js') }}
        {{ Html::script('vendors/js/Chart.min.js') }}
        {{ Html::script('js/moment.js') }}
        {{ Html::script('js/bootstrap-material-datetimepicker.js') }}
        <!-- JQuery & Ajax queries-->
        {{ Html::script('js/fieldcontents.js') }}

    </head>
    <body class="app flex-row align-items-center">
        @if(\Auth::check())
        <h5>Hola {{ auth()->user()->nombre_u }}</h5>
        @endif
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card-group">
                        <div class="card p-4">
                            <div class="card-body">
                                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    <h1>Login</h1>
                                    <p class="text-muted">Porfavor ingrese sus credenciales</p>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-user"></i></span>
                                        </div>
                                        <input type="text" value="{{ old('username') }}" name="username" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" placeholder="Username">
                                    </div>
                                    <div class="input-group mb-4">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-lock"></i></span>
                                        </div>
                                        <input type="password" name="password" value="{{ old('password') }}" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" placeholder="Password">
                                        <div class="invalid-feedback">
                                            {{ $errors->first('username',':message') }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="submit" class="btn btn-primary">Login</button>
                                        </div>
                                        <div class="col-6 text-right">
                                            <a class="btn btn-link" href="#">
                                                Forgot Your Password?
                                            </a>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                                <div class="card-body text-center">
                                    <div>
                                        <h2>Sign up</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <button type="button" class="btn btn-primary active mt-3">Register Now!</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <!-- Mirrored from coreui.io/demo/Static_Demo/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Apr 2018 00:24:22 GMT -->
</html>