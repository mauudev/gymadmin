  <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=> 'normal'], function(){
  Route::get('/', function () {
      return view('layouts.main');
  });

  Route::resource('clientes','ClientesController');
  Route::get('/clientes/{cliente}/destroy',[
        'uses'=>'ClientesController@destroy',
        'as' => 'clientes.destroy'
      ]);
  Route::get('/clientes/search/{search}','ClientesController@busquedaCliente');

  Route::resource('usuarios','UsuariosController');
  Route::get('/usuarios/{usuario}/destroy',[
        'uses'=>'UsuariosController@destroy',
        'as' => 'usuarios.destroy'
      ]);

  Route::resource('instructores','InstructoresController');
  Route::get('/instructores/{instructor}/destroy',[
        'uses'=>'InstructoresController@destroy',
        'as' => 'instructores.destroy'
      ]);

  Route::resource('disciplinas','DisciplinasController');
  Route::get('/disciplinas/{disciplina}/destroy',[
        'uses'=>'DisciplinasController@destroy',
        'as' => 'disciplinas.destroy'
      ]);

  Route::resource('horarios-disciplina','HorariosDiscController');
  Route::get('/horarios-disciplina/{horario}/destroy',[
        'uses'=>'HorariosDiscController@destroy',
        'as' => 'horarios-disciplina.destroy'
      ]);

  Route::resource('inscripciones','InscripcionesController');
  Route::get('/inscripciones/{inscripcion}/destroy',[
        'uses'=>'InscripcionesController@destroy',
        'as' => 'inscripciones.destroy'
      ]);
  //JQuery & Ajax
  Route::get('instructores-by-disciplina/{disciplina}','InstructoresController@getInstructoresByDisciplina');
  Route::get('horarios-by-disciplina/{disciplina}','HorariosDiscController@getHorariosByDiscId');
  //JQuery & Ajax

});
//Route::get('login','LoginController@showLoginForm')->name('login');
Route::get('unauthorized',[
      'uses'=>'\GymAdmin\Http\Controllers\Auth\LoginController@unauthorized',
      'as' => 'section.unauthorized'
    ]);
Route::get('logout','\GymAdmin\Http\Controllers\Auth\LoginController@logout')->name('logout');
Auth::routes();








// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

